const filterByColor = function(item) {
  return this.colors.find(color => item.colors.join(" ").includes(color))
}

const filterByPrice = function(item) {
  let price = parseFloat(item.price)
  if ( price >= this.from && (!this.to || price <= this.to) ) {
    return true
  }
  if ( item.price_varies ) {
    let price_min = parseFloat(item.price_min)
    let price_max = parseFloat(item.price_max)
    if ( price_min >= this.from && (!this.to || price_min <= this.to) ) {
      return true
    }
    if ( price_max >= this.from && (!this.to || price_max <= this.to) ) {
      return true
    }
  }
  return false
}

const filterSale = function(item) {
  if (item.onSale && !item.variant_soldout && !item.all_variants_soldout) {
    return true
  }
}

const moveToEnd = function(items, key1, key2) {
  let regularItems = [], endItems = []
  items.forEach(item => {
    let isEndItem = !item.preorder && item.badge && (item.badge.type == 'sale' || item.badge.type == 'soldout')
    let shouldBeAtEnd = item[key1] || item[key2]
    if ( isEndItem && shouldBeAtEnd ) {
      endItems.push(item)
    } else {
      regularItems.push(item)
    }
  })

  items = regularItems.concat(endItems)
  return items
}

const filterCollection = ({items, options, handle}) => {
  if (!items) return []
  
  // FILTER: sale
  if (handle && handle == 'sale') {
    let tmpItems = []
    items = items.filter(filterSale, {handle, items})
    items = items.filter(item => {
      if (item.tags.includes('show-variant')) {
        return true
      } else {
        let firstSaleItem = items.find( v => v.productId == item.productId && v.onSale)
        if (firstSaleItem && !tmpItems.find( v => v.productId == item.productId)) {
          tmpItems.push(firstSaleItem)
          return true
        }
      }
    })
  }
  
  if (options) {
    // FILTER: color
    if (options.colors.length) {
      items = items.filter(filterByColor, {colors: options.colors})
    } else if (options.hash != '') {
      items = items.filter(filterByColor, {colors: [options.hash]})
    }

    // FILTER: price
    if (options.price.length) {
      if (options.price.includes('-')) {
        var [from, to] = options.price.split("-");
        to *= 100
      }
      else if (options.price.includes('+')) {
        var [from] = options.price.split("+")
        var to = false
      }
      from *= 100
      items = items.filter(filterByPrice, {from: from, to: to})
    }
  }

  // MOVE to end:
  items = moveToEnd(items, 'onSale')
  items = moveToEnd(items, 'variant_soldout', 'all_variants_soldout')

  if (options) {
    // SORT: price
    if (options.sort == "price-ascending") {
      items.sort((a, b) => {return a.price - b.price})
    }
    else if (options.sort == "price-descending") {
      items.sort((a, b) => {return b.price - a.price})
    }
  }

  return items
}

export default filterCollection